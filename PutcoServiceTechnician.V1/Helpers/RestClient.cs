using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using PutcoServiceTechnician.V1.Models;
using System.Collections.Generic;

namespace PutcoServiceTechnician.V1.Helpers
{

    public enum httpVerb
    {
        GET,
        POST,
        PUT,
        DELETE 
    }
    class RestClient
    {
        public string endPoint { get; set; }

        public httpVerb httpMethod { get; set; }

        public string json { get; set; }

        public RestClient()
        {
            endPoint = string.Empty;
            httpMethod = httpVerb.POST;
            json = string.Empty;

        }

        public string makeRequest()
        {
            string strResponceValue = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(endPoint);

            request.Method = httpMethod.ToString();
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApplicationException("Error Code: " + response.StatusCode.ToString());
                }

                //Process the responce stream ... (Could be Json, XML or HTML etc...)

                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            strResponceValue = reader.ReadToEnd();

                        } // End of StreamReadcer
                    }
                } // End of using RespoceStream
            }

            return strResponceValue;
        }

        public string PostRequest()
        {
            var webAddr = endPoint;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                streamWriter.Write(json);
                streamWriter.Flush();
            }
            try
            {

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    
                    string str = result;
                    str = JToken.Parse(result).ToString();

                    return result;
                }

            }
            catch (Exception)
            {

                return "False";
            }

        }

        public bool PostRequestBoolean()
        {
            var webAddr = endPoint;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                streamWriter.Write(json);
                streamWriter.Flush();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string result = streamReader.ReadToEnd();

                    bool isCompleted = Convert.ToBoolean(result);
                    return isCompleted; 
                }
            }
            catch (Exception)
            {

                return false;
            }
           

        }
    }
}