using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using PutcoServiceTechnician.V1.PutcoAPI;
namespace PutcoServiceTechnician.V1.Helpers
{
    class WebConnector
    { 
            #region Properties

            /// <summary>Initialise Cherwell Web Service.</summary>
            private ServiceTechnician _webService;

            #endregion

            #region Constructor

            /// <summary>Constructor to initialise class.</summary>
            public WebConnector()
            {
            }

            #endregion

            #region Public Methods

            /// <summary>Establish a session and log in. Capture session cookie for later use.</summary>
            /// <returns>Returns true if login is successful</returns>
            internal bool Login(string username, string password)
            {
                _webService = new ServiceTechnician() { CookieContainer = new System.Net.CookieContainer() };
                return _webService.logon(username, password);
            }

           
            #endregion
    }

}
