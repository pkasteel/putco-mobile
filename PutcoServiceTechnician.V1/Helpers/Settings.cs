using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PutcoServiceTechnician
{
    class Settings
    {
        public void saveSettings(string settings, string nameSettings)
        {

            //store
            var prefs = Application.Context.GetSharedPreferences(nameSettings, FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString(nameSettings, settings);
            prefEditor.Commit();

        }

        // Function called from OnCreate
        public string retrieveSettings(string nameSettings)
        {
            //retreive 
            var prefs = Application.Context.GetSharedPreferences(nameSettings, FileCreationMode.Private);
            var settings = prefs.GetString(nameSettings, null);
            return settings;

        }
    }
}