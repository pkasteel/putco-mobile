﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System;
using System.Collections.Generic;
using PutcoServiceTechnician.V1.Helpers;
using Newtonsoft.Json;
using PutcoServiceTechnician.V1.Models;
using Newtonsoft.Json.Linq;

namespace PutcoServiceTechnician.V1
{
    [Activity(Label = "PutcoServiceTechnician", Theme = "@android:style/Theme.Light.NoTitleBar.Fullscreen",
        MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private bool loggedIn;

        private string username;
        private string password;
        private Settings UserSettings = new Settings();

#region OnCreate
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            //Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            Button buttonConnect = FindViewById<Button>(Resource.Id.loginButton); //Defining the interactionfield
            TextView txtUsername = FindViewById<TextView>(Resource.Id.usernameEdit);
            TextView txtPassword = FindViewById<TextView>(Resource.Id.passwordEdit);
            TextView txtResponce = FindViewById<TextView>(Resource.Id.loginFailed);


            txtResponce.Text = "";

            //Set Button clicked handeler
            buttonConnect.Click += delegate
            {
                username = txtUsername.Text; // Stores textbox value in value
                password = txtPassword.Text; // Stores textbox value in value
                Login(username,password,txtResponce,txtUsername,txtPassword);
            };
        }
#endregion
        private void Login(string username, string password, TextView responce, TextView Username, TextView Password)
            //Login Method
        {

            var userCheck = new UserDetails();
            userCheck.MSXUsername = username;
            userCheck.MSXPassword = password;
            var passwordcheck = JsonConvert.SerializeObject(userCheck);

            var uri = "http://192.168.1.137/PutcoCherwellService/api/Userinfo/login/";
             RestClient rClient = new RestClient();
            rClient.endPoint = uri;
            rClient.json = passwordcheck;
            loggedIn = rClient.PostRequestBoolean();

            try
            {
                if (loggedIn)
                    //Evaluate if login was succesfull via Bool variable. If true then runs code inside the block
                {

                    uri = "http://192.168.1.137/PutcoCherwellService/api/Userinfo/getUser/";
                    rClient.endPoint = uri;
                    string user = string.Empty;
                    user = rClient.PostRequest();
                    string str = user;
                    str = JToken.Parse(user).ToString(); ;//myConnection.getUserInfo(username); //Fetches Userdetails from Webservice
                    {
                       UserSettings.saveSettings(str, "userDetails"); //Stores user Settings for later use
                        var CheckAccount = new List<UserDetails>(JsonConvert.DeserializeObject<List<UserDetails>>(str));

                        if (CheckAccount[0].Department.ToUpper() == "FOREMAN")
                        {
                            var myIntent = new Intent(this, typeof(myWork)); //Initiates new Intent of activity
                            StartActivity(myIntent); //Starts new activity "MyWork"
                        }
                        else
                        {
                            var myIntent =  new Intent(this,typeof(myWork));
                            StartActivity(myIntent);
                        }

                    }
                }
                else
                {
                    Username.Text = "";
                    Password.Text = "";
                    changeTextview(null, null, responce,
                        "Username/password incorect or you have not been set up as mobile app user! Please conact you adminstrator");
                    // If login was unsucessfull displays message on screen
                }


            }
            catch (Exception)
            {
                Username.Text = "";
                Password.Text = "";
                changeTextview(null, null, responce,
                    "No connection with the server could be established, please check your connectivity");

            }

        }

        private void changeTextview(object sender, EventArgs e, TextView response, string responseText)
            //Processes and displays login message
        {
            response.Text = responseText;

        }
       

    }

}
