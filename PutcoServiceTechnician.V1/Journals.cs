using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PutcoServiceTechnician.V1.Adapters;
using PutcoServiceTechnician.V1.Models;
using Newtonsoft.Json;
using PutcoServiceTechnician.V1.Helpers;
using Newtonsoft.Json.Linq;

namespace PutcoServiceTechnician.V1
{
    [Activity(Label = "Journals", Theme = "@android:style/Theme.Light.NoTitleBar.Fullscreen")]
    public class Journals : Activity
    {
        private ListView listView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var incidentDetails = Intent.GetStringExtra("Incident");
            var incident = JsonConvert.DeserializeObject<IncidentDetails>(incidentDetails);



            List<JournalDetails> JournalList = loadJournals(incident);

            SetContentView(Resource.Layout.Journals);

            TextView txtHeader = FindViewById<TextView>(Resource.Id.txtJournalsHeader);

            txtHeader.Text = incident.IncidentID + " " + incident.ConfigItemDisplayName;
            listView = FindViewById<ListView>(Resource.Id.JournalListviews); // Set loistview to resource
            listView.Adapter = new JournalsAdapters(this, JournalList); // Link Listview to listview Adapter

            //listView.ItemClick += OnListItemClick; // Calls on click a methode (OnListItemClick) 


            // Create your application here
        }

        private List<JournalDetails> loadJournals(IncidentDetails RecID)
        {
            List<JournalDetails> allJournals;

            try
            {
                var uri = "http://192.168.1.137/PutcoCherwellService/api/Journals/getJournals/";
                RestClient rClient = new RestClient();
                rClient.endPoint = uri;
                var json = JsonConvert.SerializeObject(RecID);
                rClient.json = json;
                string strResponse = rClient.PostRequest();
                string str = strResponse;
                str = JToken.Parse(str).ToString();

                allJournals = JsonConvert.DeserializeObject<List<JournalDetails>>(str);
            }
            catch (System.Exception)
            {

                throw;
            }

            return allJournals;


        }

    }
}