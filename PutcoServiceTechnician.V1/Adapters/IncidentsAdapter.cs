using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Views;
using Android.Widget;
using PutcoServiceTechnician.V1.Models;

namespace PutcoServiceTechnician.V1.Adapters
{
   internal class IncidentAdapter : BaseAdapter<IncidentDetails>
    {
   List<IncidentDetails> items;
   Activity context;
   public IncidentAdapter(Activity context, List<IncidentDetails> items): base()
   {
	   this.context = context;
	   this.items = items;
   }
   public override long GetItemId(int position)
   {
	   return position;
   }
   public override IncidentDetails this[int position]
   {
	   get { return items[position]; }
   }
   public override int Count
   {
	   get { return items.Count; }
   }
   public override View GetView(int position, View convertView, ViewGroup parent)
   {
	   var item = items[position];
	   View view = convertView;
	   if (view == null) // no view to re-use, create new
		   view = context.LayoutInflater.Inflate(Resource.Layout.IncidentListview, null);
	        view.FindViewById<TextView>(Resource.Id.txtIncidentID).Text = item.IncidentID;
            view.FindViewById<TextView>(Resource.Id.txtStatus).Text = item.Status;
            view.FindViewById<TextView>(Resource.Id.txtservice).Text = item.Subcategory;
            view.FindViewById<TextView>(Resource.Id.txtDetails).Text = item.ConfigItemDisplayName;

       return view;
   }
}
}