using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Views;
using Android.Widget;
using PutcoServiceTechnician.V1.Models;

namespace PutcoServiceTechnician.V1.Adapters
{
   internal class JournalsAdapters : BaseAdapter<JournalDetails>
    {
   List<JournalDetails> items;
   Activity context;
   public JournalsAdapters(Activity context, List<JournalDetails> items): base()
   {
	   this.context = context;
	   this.items = items;
   }
   public override long GetItemId(int position)
   {
	   return position;
   }
   public override JournalDetails this[int position]
   {
	   get { return items[position]; }
   }
   public override int Count
   {
	   get { return items.Count; }
   }
   public override View GetView(int position, View convertView, ViewGroup parent)
   {
	   var item = items[position];
	   View view = convertView;
	   if (view == null) // no view to re-use, create new
		   view = context.LayoutInflater.Inflate(Resource.Layout.JournalListView, null);
            view.FindViewById<TextView>(Resource.Id.txtJournalListDateTime).Text = item.CreatedDateTime.ToString();
            view.FindViewById<TextView>(Resource.Id.txtJournalListOwner).Text = item.CreatedBy.ToString();
            if (item.JournalTypeName != null){view.FindViewById<TextView>(Resource.Id.txtJournalListEntryType).Text = item.JournalTypeName.ToString();}
            view.FindViewById<TextView>(Resource.Id.txtJournalListText).Text = item.Details.ToString();

       return view;
   }
}
}