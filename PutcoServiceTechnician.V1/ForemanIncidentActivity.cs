
using System;
using System.Collections.Generic;
using System.Text;
using Android.App;
using Android.OS;
using Android.Widget;
using Newtonsoft.Json;
using PutcoServiceTechnician.V1.Models;
using System.Web.Services.Protocols;
using PutcoServiceTechnician.V1.PutcoAPI;
using PutcoServiceTechnician.V1.Helpers;
using Android.Graphics;
using Android.Content;
using Newtonsoft.Json.Linq;

namespace PutcoServiceTechnician.V1
{
    [Activity(Label = "ForemanIncidentActivity", Theme= "@android:style/Theme.Light.NoTitleBar.Fullscreen")]
    public class ForemanIncidentActivity : Activity
    {


        protected override void OnCreate(Bundle savedInstanceState)
        {
            Settings userSettings = new Settings();
            var myRestClient = new RestClient();
            var incidentDetails = Intent.GetStringExtra("Incident_Details");
            var userListString = userSettings.retrieveSettings("userDetails");
            var user = JsonConvert.DeserializeObject<List<UserDetails>>(userListString);
            var incident = JsonConvert.DeserializeObject<IncidentDetails>(incidentDetails);
            var configuration = JsonConvert.DeserializeObject<ConfigItems>(JToken.Parse(LoadConfigItems(incident)).ToString());
            //var Tasks = JsonConvert.DeserializeObject()
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.IncidentLayout);

            //Defining the interactionfield
            Button buttonNew = FindViewById<Button>(Resource.Id.btnIncidentDetailNew);
            Button buttonAssigned = FindViewById<Button>(Resource.Id.btnIncidentDetailAssigned);
            Button buttonBeginWork = FindViewById<Button>(Resource.Id.btnIncidentDetailBeginWork);
            Button buttonBeginInProgress = FindViewById<Button>(Resource.Id.btnIncidentDetailInProgress);
            Button buttonBeginPending = FindViewById<Button>(Resource.Id.btnIncidentDetailPending);
            Button buttonBeginResolved = FindViewById<Button>(Resource.Id.btnIncidentDetailResolved);
            Button buttonBeginReopend = FindViewById<Button>(Resource.Id.btnIncidentDetailReopened);
            ImageButton buttonBusDetails = FindViewById<ImageButton>(Resource.Id.btnImageBus);
            ImageButton buttonConfiguration = FindViewById<ImageButton>(Resource.Id.btnImageGears);
            ImageButton buttonLubrication = FindViewById<ImageButton>(Resource.Id.btnImageLubricants);
            ImageButton buttonJournals = FindViewById<ImageButton>(Resource.Id.btnImageJournals);
            ImageButton buttonAttachements = FindViewById<ImageButton>(Resource.Id.btnImageAttachments);
            TextView txtIncidentId = FindViewById<TextView>(Resource.Id.txtIncDetailJobNumber);
            TextView txtStatus = FindViewById<TextView>(Resource.Id.txtIncDetailStatus);
            TextView txtDescription = FindViewById<TextView>(Resource.Id.txtIncDetailDescription);
            TextView txtService = FindViewById<TextView>(Resource.Id.txtIncDetailService);
            TextView txtCategory = FindViewById<TextView>(Resource.Id.txtIncDetailCategory);
            TextView txtSubCategory = FindViewById<TextView>(Resource.Id.txtIncDetailSubCategory);
            TextView txtBusDetails = FindViewById<TextView>(Resource.Id.txtIncDetailSBusDetails);
            TextView txtRemarks = FindViewById<TextView>(Resource.Id.txtIncidentDetailsRemarks);
            TextView txtTotalTasks = FindViewById<TextView>(Resource.Id.txtTotalTasks);
            TextView txtOpenTasks= FindViewById<TextView>(Resource.Id.txtOpenTasks);
            TextView txtPendingTasks = FindViewById<TextView>(Resource.Id.txtPendingTasks);
            TextView txtClosedTasks= FindViewById<TextView>(Resource.Id.txtClosedTasks);

            buttonNew.Click += delegate
            {
                onButtonNewClick(incident,user[0]);
            };
   
            buttonAssigned.Click += delegate
            {
                onButtonAssignClick(incident, user[0]);
            };

            buttonBeginWork.Click += delegate
            {
                onButtonBeginWork(incident, user[0]);
            };

            buttonBeginInProgress.Click += delegate
            {
                onButtonInProgressClick(incident, user[0]);
            };

            buttonBeginPending.Click += delegate
            {
                onButtonInPending(incident, user[0]);
            };

            buttonBeginReopend.Click += delegate
            {
                onButtonReopened(incident, user[0]);
            };

            buttonBeginResolved.Click += delegate
            {
                onButtonResolved(incident, user[0]);
            };

            buttonBusDetails.Click += delegate
            {
                OnButtonBusclick(configuration);
            };

            buttonConfiguration.Click += delegate
            {
                OnButtonGearclick(configuration);
            };

            buttonLubrication.Click += delegate
            {
                OnButtonLubricationClick(configuration);
            };

            buttonJournals.Click += delegate
            {
                OnButtonJournalsClick(incidentDetails);
            };

            buttonAttachements.Click += delegate
            {

            };
            txtTotalTasks.Click += delegate
            {

            };

            txtOpenTasks.Click += delegate
            {

            };

            txtPendingTasks.Click += delegate
            {

            };

            txtClosedTasks.Click += delegate
            {

            };

            txtIncidentId.Text = incident.IncidentID;
            txtStatus.Text = incident.Status;
            txtDescription.Text = incident.ShortDescription;
            txtService.Text = incident.Service;
            txtCategory.Text = incident.Category;
            txtSubCategory.Text = incident.Subcategory;
            txtBusDetails.Text = incident.ConfigItemDisplayName;
            txtRemarks.Text = incident.Description;

            assignButtonHighlights(incident.Status.ToString());
        }


        private void onButtonNewClick(IncidentDetails incident, UserDetails user)
        {
            updateIncident(incident, user, "New");

        }

        private void onButtonAssignClick(IncidentDetails incident, UserDetails user)
        {
            updateIncident(incident, user, "Assigned");
        }

        private void onButtonBeginWork(IncidentDetails incident, UserDetails user)
        {
            updateIncident(incident, user, "Begin Work");

        }

        private void onButtonInProgressClick(IncidentDetails incident, UserDetails user)
        {
            updateIncident(incident, user, "In Progress");

        }

        private void onButtonInPending(IncidentDetails incident, UserDetails user)
        {
            updateIncident(incident, user, "Pending");


        }

        private void onButtonResolved(IncidentDetails incident, UserDetails user)
        {
            updateIncident(incident, user, "Resolved");

        }

        private void onButtonReopened(IncidentDetails incident, UserDetails user)
        {
            updateIncident(incident, user, "Reopened");

        }

        private void OnButtonBusclick(ConfigItems mybus)
        {
            StringBuilder Busstring =new StringBuilder();
            Busstring.AppendLine("Bus Type: " + mybus.Manufacturer);
            Busstring.AppendLine("Bus Model: " + mybus.Model);
            Busstring.AppendLine("Fleet Number: " + mybus.FleetNumber);
            Busstring.AppendLine("Registration Number: " + mybus.RegistrationNumber);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(mybus.Description);
            alert.SetMessage(Busstring.ToString());
            alert.SetPositiveButton("OK", (senderAlert, args) => {
            });
            Dialog dialog = alert.Create();
            dialog.Show();
        }

        private void OnButtonGearclick(ConfigItems mybus)
        {
            StringBuilder Busstring = new StringBuilder();
            Busstring.AppendLine("Engine Placement " + mybus.EnginePlacement);
            Busstring.AppendLine("GearBox Code: " + mybus.GearboxCode);
            Busstring.AppendLine("Configuration: " + mybus.Configuration);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(mybus.Description);
            alert.SetMessage(Busstring.ToString());
            alert.SetPositiveButton("OK", (senderAlert, args) => {
            });
            Dialog dialog = alert.Create();
            dialog.Show();
        }

        private void OnButtonLubricationClick(ConfigItems mybus)
        {
            StringBuilder Busstring = new StringBuilder();
            Busstring.AppendLine("Drive Axle Oil: " + mybus.DriveAxleOil);
            Busstring.AppendLine("GearBox Oil: " + mybus.GearboxOil);
            Busstring.AppendLine("Engine Oil: " + mybus.EngineOil);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(mybus.Description);
            alert.SetMessage(Busstring.ToString());
            alert.SetPositiveButton("OK", (senderAlert, args) => {
            });
            Dialog dialog = alert.Create();
            dialog.Show();

        }

        private void OnButtonJournalsClick(string RecID)
        {
            var myIntent = new Intent(this, typeof(Journals));
            myIntent.PutExtra("Incident", RecID);
            StartActivity(myIntent);
        }

        private void updateIncident(IncidentDetails incident, UserDetails user, string status)
        {
            incident.Status = status;
            incident.LastModifiedDateTime = DateTime.Now;
            incident.LastModBy = user.FullName.ToString();
            incident.LastModByID = user.RecID.ToString();
            var json = JsonConvert.SerializeObject(incident);
            var uri = "http://192.168.1.137/PutcoCherwellService/api/Incidents/updateIncident/";
            RestClient rClient = new RestClient();
            rClient.endPoint = uri;
            rClient.json = json;
            bool result = rClient.PostRequestBoolean();
            if (result==false)
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Error!!!");
                alert.SetMessage("Something went wrong with changing the status of the Job.\n" +
                    "Please check you connectivity or contact your Supervisor");
                alert.SetPositiveButton("OK", (senderAlert, args) => {
                });
                Dialog dialog = alert.Create();
                dialog.Show();
            }
            else
            {
                TextView txtStatus = FindViewById<TextView>(Resource.Id.txtIncDetailStatus);
                txtStatus.Text = status.ToString();
                assignButtonHighlights(status);
                Toast.MakeText(this, incident.IncidentID.ToString() + " Successfully Updated", ToastLength.Long).Show();
            }
        }

        private void assignButtonHighlights(string status)
        {
            Button buttonNew = FindViewById<Button>(Resource.Id.btnIncidentDetailNew);
            Button buttonAssigned = FindViewById<Button>(Resource.Id.btnIncidentDetailAssigned);
            Button buttonBeginWork = FindViewById<Button>(Resource.Id.btnIncidentDetailBeginWork);
            Button buttonBeginInProgress = FindViewById<Button>(Resource.Id.btnIncidentDetailInProgress);
            Button buttonBeginPending = FindViewById<Button>(Resource.Id.btnIncidentDetailPending);
            Button buttonBeginResolved = FindViewById<Button>(Resource.Id.btnIncidentDetailResolved);
            Button buttonBeginReopend = FindViewById<Button>(Resource.Id.btnIncidentDetailReopened);

            buttonNew.Clickable = false;
            buttonNew.SetTextColor(Color.ParseColor("#000000"));
            buttonAssigned.Clickable = false;
            buttonAssigned.SetTextColor(Color.ParseColor("#000000"));
            buttonBeginWork.Clickable = true;
            buttonBeginWork.SetTextColor(Color.ParseColor("#000000"));
            buttonBeginInProgress.Clickable = false;
            buttonBeginInProgress.SetTextColor(Color.ParseColor("#000000"));
            buttonBeginPending.Clickable = false;
            buttonBeginPending.SetTextColor(Color.ParseColor("#000000"));
            buttonBeginResolved.Clickable = false;
            buttonBeginResolved.SetTextColor(Color.ParseColor("#000000"));
            buttonBeginReopend.Clickable = false;
            buttonBeginReopend.SetTextColor(Color.ParseColor("#000000"));

            switch (status)
            {
                case "New":
                    buttonBeginWork.Clickable = true;
                    buttonBeginWork.SetTextColor(Color.ParseColor("#ffffff"));
                    break;
                case "Assigned":
                    buttonBeginWork.Clickable = true;
                    buttonBeginWork.SetTextColor(Color.ParseColor("#ffffff"));
                    break;
                case "Begin Work":
                    buttonBeginInProgress.Clickable = true;
                    buttonBeginInProgress.SetTextColor(Color.ParseColor("#ffffff"));

                    break;
                case "In Progress":
                    buttonBeginResolved.SetTextColor(Color.ParseColor("#ffffff"));
                    buttonBeginPending.SetTextColor(Color.ParseColor("#ffffff"));
                    buttonBeginPending.Clickable = true;
                    buttonBeginResolved.Clickable = true;

                    break;
                case "Pending":
                    buttonBeginInProgress.Clickable = true;
                    buttonBeginInProgress.SetTextColor(Color.ParseColor("#ffffff"));
                    break;
                case "Resolved":
                    buttonBeginReopend.Clickable = true;
                    buttonBeginReopend.SetTextColor(Color.ParseColor("#ffffff"));

      
                    break;
                case "Reopened":
                    buttonBeginWork.Clickable = true;
                    buttonBeginWork.SetTextColor(Color.ParseColor("#ffffff"));
                    break;
            }

        }

        private string LoadConfigItems(IncidentDetails RecID)
        {
            try
            {
                var uri = "http://192.168.1.137/PutcoCherwellService/api/ConfigurationItems/GetConfigurationItem/";
                var json = JsonConvert.SerializeObject(RecID);
                RestClient rClient = new RestClient();
           
                rClient.endPoint = uri;
                rClient.json = json;
                string strResponse = rClient.PostRequest();
                string str = strResponse;
                str = JToken.Parse(str).ToString();

                return str;
            }
            catch (System.Exception)
            {

                throw;
            }
        }

    }
}