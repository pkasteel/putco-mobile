namespace PutcoServiceTechnician.V1.Models
{
    class Tasks
    {
        public string RecID { get; set; }
        public string OwnedByTeam { get; set; }
        public string OwnedByTeamID { get; set; }
        public string OwnedBy { get; set; }
        public string OwnedByID { get; set; }
        public string TaskID { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public System.Nullable<System.DateTime> StartDateTime { get; set; }
        public string ParentRecID { get; set; }
        public string AssignedToID { get; set; }
        public string AssignedTo { get; set; }
        public string AssignedToTeamID { get; set; }
        public string AssignedToTeam { get; set; }
        public string Notes { get; set; }
        public string AcceptedByID { get; set; }
        public string AcknowledgedBy { get; set; }
        public System.Nullable<System.DateTime> AcknowledgedDateTime { get; set; }
        public string AcknowledgedComment { get; set; }
        public string ResolvedByID { get; set; }
        public string ResolvedBy { get; set; }
        public System.Nullable<System.DateTime> ResolvedDateTime { get; set; }
        public string CloseCode { get; set; }
        public string CompletionDetails { get; set; }
        public System.Nullable<decimal> TaskDurationInHours { get; set; }
        public string Availabiltiy { get; set; }
        public string Email { get; set; }
        public string LimitAssigneeByTeam { get; set; }
        public string ParentTypeName { get; set; }
        public string ParentPublicID { get; set; }
        public System.Nullable<decimal> TaskOrder { get; set; }
        public string ParentActivity { get; set; }
        public System.Nullable<int> ActivityOrder { get; set; }
        public System.Nullable<int> ParentActivityOrder { get; set; }
        public System.Nullable<bool> TaskActive { get; set; }
        public string Name { get; set; }
        public System.Nullable<System.DateTime> EndDateTime { get; set; }
        public string Location { get; set; }
        public string Title { get; set; }
        public string ClosedBy { get; set; }
        public string ClosedByID { get; set; }
        public System.Nullable<System.DateTime> ClosedDateTime { get; set; }
        public string Signoff { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public string NextStatus { get; set; }
        public string NextStatusText { get; set; }
        public string StatusID { get; set; }
        public string SelectedStatus { get; set; }
        public string HoldReason { get; set; }
        public System.Nullable<System.DateTime> HoldReviewDate { get; set; }
        public System.Nullable<System.DateTime> HoldDate { get; set; }
        public string ReassignedTo { get; set; }
        public string ReassignedToTeam { get; set; }
        public string ReassignedToTeamID { get; set; }
    }
}