namespace PutcoServiceTechnician.V1.Models
{
    /// <summary>
    /// This Object Class is used for storing and retrieving all items from the Json string (UserDetails)
    /// </summary>
    public class UserDetails
    {
        public string RecID { get; set; }
        public System.Nullable<System.DateTime> LastModifiedDateTime { get; set; }
        public string LastModBy { get; set; }
        public string LastModifiedByID { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string DefaultTeamName { get; set; }
        public string DefaultTeamID { get; set; }
        public bool MSXMobileAppUser { get; set; }
        public string MSXPassword { get; set; }
        public string MSXUsername { get; set; }
    }
}