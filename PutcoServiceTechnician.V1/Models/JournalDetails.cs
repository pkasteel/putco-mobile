
namespace PutcoServiceTechnician.V1.Models
{
    class JournalDetails
    {
        public string RecID { get; set; }
        public string JournalTypeID { get; set; }
        public string JournalTypeName { get; set; }
        public System.Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByID { get; set; }
        public System.Nullable<System.DateTime> LastModifiedDateTime { get; set; }
        public string LastModBy { get; set; }
        public string LastModByID { get; set; }
        public string ParentTypeID { get; set; }
        public string ParentRecID { get; set; }
        public string Details { get; set; }
        public System.Nullable<bool> ShowInSelfService { get; set; }
        public string SubParentID { get; set; }
        public System.Nullable<bool> MarkedAsRead { get; set; }
        public string CreatedCulture { get; set; }
        public string OwnedByTeam { get; set; }
        public string OwnedByTeamID { get; set; }
        public string OwnedBy { get; set; }
        public string OwnedByID { get; set; }
        public string HistoryType { get; set; }
        public string AuditFieldName { get; set; }
        public string AuditFieldOldValue { get; set; }
        public string AuditFieldNewValue { get; set; }
        public string AuditFieldID { get; set; }
        public string CommentID { get; set; }
        public string Quote { get; set; }
        public string QuotedComment { get; set; }
        public string QuotedComment_Html { get; set; }
        public string QuotedAuthor { get; set; }
        public System.Nullable<System.DateTime> QuotedDateTime { get; set; }
        public System.Nullable<bool> EditMode { get; set; }
        public string RequestType { get; set; }
        public string Priority { get; set; }
        public string QuickJournal { get; set; }
        public string QueueScope { get; set; }
        public string QueueScopeOwner { get; set; }
        public string QueueID { get; set; }
        public string QueueDisplayName { get; set; }
        public string QueueOperationCode { get; set; }
        public string EmailID { get; set; }
        public string MailDirection { get; set; }
        public string ToAddress { get; set; }
        public string FromAddress { get; set; }

        public string ChatConversation { get; set; }

        public string ChatStartTime { get; set; }

        public string ChatEventDetails { get; set; }

    }
}