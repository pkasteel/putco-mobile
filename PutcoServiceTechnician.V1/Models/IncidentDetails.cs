namespace PutcoServiceTechnician.V1.Models
{
    /// <summary>
    /// This Object Class is used for storing and retrieving all items from the Json string (Incident Details)
    /// </summary>
    public class IncidentDetails
    {
        public string RecID { get; set; }
        public string IncidentID { get; set; }
        public System.Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string Status { get; set; }
        public string StatusDesc { get; set; }
        public string Service { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public string Description { get; set; }
        public string Description_Html { get; set; }
        public string Priority { get; set; }
        public string OwnedByTeam { get; set; }
        public string OwnedByTeamID { get; set; }
        public string OwnedBy { get; set; }
        public string OwnedByID { get; set; }
        public string CloseDescription { get; set; }
        public string CloseDescription_Html { get; set; }
        public System.Nullable<System.DateTime> LastModifiedDateTime { get; set; }
        public string LastModBy { get; set; }
        public string LastModByID { get; set; }
        public string ConfigItemTypeID { get; set; }
        public string ConfigItemRecID { get; set; }
        public string IncidentType { get; set; }
        public string Detail { get; set; }
        public string PendingReason { get; set; }
        public string Task { get; set; }
        public string Notes { get; set; }
        public string NoteEntry { get; set; }
        public string ConfigItemDisplayName { get; set; }
        public System.Nullable<decimal> TotalTasks { get; set; }
        public string NextStatus { get; set; }
        public string NextStatusText { get; set; }
        public string NextStatusOneStep { get; set; }
        public string SelectedStatus { get; set; }
        public string StatusID { get; set; }
        public string SubcategoryID { get; set; }
        public System.Nullable<bool> HasNoOpenTasks { get; set; }
        public string Cause { get; set; }
        public string Comments { get; set; }
        public string Comments_Html { get; set; }
        public string CommentsEntry { get; set; }
        public string ShortDescription { get; set; }
        public System.Nullable<bool> TasksInProgress { get; set; }
        public bool TasksClosed { get; set; }
        public System.Nullable<int> TaskClosedCount { get; set; }
        public bool TasksOnHold { get; set; }
    }
}