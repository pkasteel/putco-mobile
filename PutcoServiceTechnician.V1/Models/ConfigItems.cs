namespace PutcoServiceTechnician.V1.Models
{
    class ConfigItems
    {
        public string RecID { get; set; }
        public string ConfigurationItemTypeID { get; set; }
        public string ConfigurationItemTypeName { get; set; }
        public string RegistrationNumber { get; set; }
        public string Vendor { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Description { get; set; }
        public string FleetNumber { get; set; }
        public string ParentRecID { get; set; }
        public string AssetStatus { get; set; }
        public string AssetType { get; set; }
        public string EngineOil { get; set; }
        public string DriveAxleOil { get; set; }
        public string GearboxOil { get; set; }
        public string No { get; set; }
        public string SerC { get; set; }
        public string EnginePlacement { get; set; }
        public string GearboxCode { get; set; }
        public string Configuration { get; set; }
        public string TypeCode { get; set; }
        public string ServiceM1ID { get; set; }
        public string ServiceM12ID { get; set; }
        public string ServiceM24ID { get; set; }
        public string ServiceM6ID { get; set; }
        public string ServiceS1ID { get; set; }
    }
}