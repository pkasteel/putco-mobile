using Android.App;
using Android.OS;
using Android.Widget;
using Newtonsoft.Json; // To be used for Serialize and Deserialize Strings received from WebService
using System.Collections.Generic;
using PutcoServiceTechnician.V1.Models; // Holds all objects
using PutcoServiceTechnician.V1.Adapters; // Holds Listview Adapters
using Android.Content;
using PutcoServiceTechnician.V1.Helpers;
using Newtonsoft.Json.Linq;

namespace PutcoServiceTechnician.V1
{
    /// <summary>
    /// 
    /// </summary>
    [Activity(Label = "myWork", Theme = "@android:style/Theme.Light.NoTitleBar.Fullscreen")]
    public class myWork : Activity
    {
        private string userListString;  //Initiated to receive the stored logged in user from the login screen via intent
        private List<IncidentDetails> Incidents; //List Object that holds all the incidents. Get's filed by Newsoft.JSON Deserialize to list
        private List<UserDetails> user; // Hold the User object. Gets filed by Newsoft Json as list
        private ListView listView; // Listview for all incidents


        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            Settings userSettings = new Settings(); // Initate user to be filled by Json deserialize


            userListString = userSettings.retrieveSettings("userDetails"); //Fetch Userlist string from MainActivity
            Button buttonMyWork = FindViewById<Button>(Resource.Id.myWork); //Set button to resource
 

            loadIncidents();

            SetContentView(Resource.Layout.MyWork);

            listView = FindViewById<ListView>(Resource.Id.listIncidents); // Set loistview to resource
            listView.Adapter = new IncidentAdapter (this, Incidents); // Link Listview to listview Adapter

            listView.ItemClick += OnListItemClick; // Calls on click a methode (OnListItemClick) 

        }

        protected void onResume()
        {

            loadIncidents();

            SetContentView(Resource.Layout.MyWork);

            listView = FindViewById<ListView>(Resource.Id.listIncidents); // Set loistview to resource
            listView.Adapter = new IncidentAdapter(this, Incidents); // Link Listview to listview Adapter

            listView.ItemClick += OnListItemClick; // Calls on click a methode (OnListItemClick) 
        }

        protected override void OnRestart()
        {
            base.OnRestart();

            loadIncidents();

            SetContentView(Resource.Layout.MyWork);

            listView = FindViewById<ListView>(Resource.Id.listIncidents); // Set loistview to resource
            listView.Adapter = new IncidentAdapter(this, Incidents); // Link Listview to listview Adapter

            listView.ItemClick += OnListItemClick; // Calls on click a methode (OnListItemClick) 
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e) // Methode that performs action when a specific item in the list has been clicked
        {

            IncidentDetails t = Incidents[e.Position]; //Stores the linked listview with assigned object to the variable
            if (t != null)
            {
                Toast.MakeText(this, t.IncidentID, ToastLength.Long).Show(); // Shows a quick Popup with the selected Incident Number
                var json = JsonConvert.SerializeObject(t);// converting to string to be passed to new intent
                var myIntent = new Intent(this, typeof(ForemanIncidentActivity)); //Initiates new Intent of activity
                myIntent.PutExtra("Incident_Details" ,json);
                StartActivity(myIntent); //Starts new activity "MyWork"
            }
        }


        private void onButtonRefreshclick()
        {

        }

        private void loadIncidents()
        {
            try
            {
                var uri = "http://192.168.1.137/PutcoCherwellService/api/Incidents/GetMyIncidents/";
                user = JsonConvert.DeserializeObject<List<UserDetails>>(userListString);
                RestClient rClient = new RestClient();
                rClient.endPoint = uri;
                var json = JsonConvert.SerializeObject(user[0]);
                rClient.json = json;
                string strResponse = rClient.PostRequest();
                string str = strResponse;
                str = JToken.Parse(str).ToString();

                Incidents = JsonConvert.DeserializeObject<List<IncidentDetails>>(str);
            }
            catch (System.Exception)
            {

                throw;
            }

            SetContentView(Resource.Layout.MyWork);

            listView = FindViewById<ListView>(Resource.Id.listIncidents); // Set loistview to resource
            listView.Adapter = new IncidentAdapter(this, Incidents); // Link Listview to listview Adapter
        }


    }
}